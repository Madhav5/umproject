from django.urls import path
from .views import *


app_name = "umapp"
urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path("register/", SignupView.as_view(), name="signup"),
    path("private/", ContactView.as_view(), name="contact"),
    path("article/", ArticleCreateView.as_view(), name="articlecreate"),
    path("articlelist/", ArticleListView.as_view(), name="articlelist"),
    path("article/<int:pk>/detail/",
         ArticleDetailView.as_view(), name="articledetail"),
    path("article/<int:pk>/update/",
         ArticleUpdateView.as_view(), name='articleupdate'),
    path("article/<art_id>/comment",
         CommentCreateView.as_view(), name='commentcreate'),
    path("ajax_check_username/", AjaxCheckUsernameView.as_view(),
         name='ajaxcheckusername'),


    path('api/v1/article-list/', ArticleListAPIView.as_view(), name="articlelistapi"),




]
