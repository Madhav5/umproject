
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import *
from .models import *
from .forms import *
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse, reverse_lazy
from django.http import JsonResponse


class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            pass
        else:
            return redirect("umapp:login")

        return super().dispatch(request, *args, **kwargs)


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "home.html"


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = "/"

    def form_valid(self, form):
        x = form.cleaned_data["username"]
        y = form.cleaned_data["password"]
        # print(x,y)
        usr = authenticate(username=x, password=y)
        # print(usr)
        if usr is not None:
            login(self.request, usr)

        else:
            return render(self.request, 'login.html',
                          {
                              "error": "Invalid Credential",
                              "form": form
                          })

        return super().form_valid(form)

    def get_success_url(self):

        if self.request.user.is_superuser:
            return "/"
        else:
            return "/private/"


class LogoutView(View):
    def get(self, request):
        logout(request)

        return redirect('/')


class SignupView(FormView):
    template_name = "signup.html"
    form_class = SignupForm
    success_url = "/"

    def form_valid(self, form):
        x = form.cleaned_data["username"]
        e = form.cleaned_data["email"]
        y = form.cleaned_data["password"]

        usr = User.objects.create_user(x, e, y)
        login(self.request, usr)

        return super().form_valid(form)


class ContactView(TemplateView):
    template_name = "contact.html"


class ArticleCreateView(LoginRequiredMixin, CreateView):
    template_name = 'articlecreate.html'
    form_class = ArticleForm
    success_url = reverse_lazy('umapp:home')

    def form_valid(self, form):
        author = self.request.user
        form.instance.author = author

        return super().form_valid(form)


class ArticleListView(ListView):
    template_name = "articlelist.html"
    queryset = Article.objects.all().order_by("-id")
    context_object_name = "allarticles"


class ArticleDetailView(DetailView):
    template_name = "articledetail.html"
    model = Article
    context_object_name = "articleobject"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["abc"] = CommentForm

        return context


class ArticleUpdateView(UpdateView):
    template_name = "articlecreate.html"
    form_class = ArticleForm
    success_url = "/articlelist/"
    model = Article
    queryset = Article.objects.all()


class CommentCreateView(LoginRequiredMixin, CreateView):
    template_name = 'commentcreate.html'
    form_class = CommentForm
    success_url = reverse_lazy('umapp:articlelist')

    def form_valid(self, form):
        art_id = self.kwargs['art_id']
        artobj = Article.objects.get(id=art_id)
        author = self.request.user
        form.instance.article = artobj
        form.instance.commentor = author

        return super().form_valid(form)

    def get_success_url(self):
        article_id = self.kwargs["art_id"]

        return reverse("umapp:articledetail", kwargs={"pk": article_id})


class AjaxCheckUsernameView(View):
    def get(self, request, *args, **kwargs):
        u = request.GET['username']
        if User.objects.filter(username=u).exists():
            message = "unavailable"

        else:
            message = "available"

        return JsonResponse({
            "message": message
        })


from rest_framework.generics import ListAPIView
from .serializers import *


class ArticleListAPIView(ListAPIView):
    serializer_class = ArticleSerializer
    queryset = Article.objects.order_by('-id')
